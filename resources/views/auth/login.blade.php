@extends('layouts.login')

@section('content')

<!-- start: page -->
<section class="body-sign">
    <div class="center-sign">
        <a href="{{url('/')}}" class="logo pull-left">
            <img src="{{asset('assets/images/' . getSetting('logo'))}}" height="54" alt="{{config('app.name')}}" />
        </a>

        <div class="panel panel-sign">
            <div class="panel-title-sign mt-xl text-right">
                <h2 class="title text-uppercase text-weight-bold m-none"><i class="fa fa-user mr-xs"></i> {{trans('general.signin')}}</h2>
            </div>
            <div class="panel-body">
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group mb-lg">
                        <label>{{trans('general.email')}}</label>
                        <div class="input-group input-group-icon">
                            <input id="email" type="email" class="form-control input-lg{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                            <span class="input-group-addon">
                                <span class="icon icon-lg">
                                    <i class="fa fa-user"></i>
                                </span>
                            </span>
                            @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group mb-lg">
                        <div class="clearfix">
                            <label class="pull-left">{{trans('general.password')}}</label>
                            @if (Route::has('password.request'))                            
                            <a href="{{ route('password.request') }}" class="pull-right">{{trans('general.forgot_your_password')}}?</a>
                            @endif                            
                        </div>
                        <div class="input-group input-group-icon">
                            <input id="password" type="password" class="form-control input-lg{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                            <span class="input-group-addon">
                                <span class="icon icon-lg">
                                    <i class="fa fa-lock"></i>
                                </span>
                            </span>
                            @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif                            
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-8">
                            <div class="checkbox-custom checkbox-default">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label class="form-check-label" for="remember">{{ trans('general.remember_me') }}</label>                                
                            </div>
                        </div>
                        <div class="col-sm-4 text-right">
                            <button type="submit" class="btn btn-primary hidden-xs">{{trans('general.signin')}}</button>
                            <button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">{{trans('general.signin')}}</button>
                        </div>
                    </div>

<!--                    <span class="mt-lg mb-lg line-thru text-center text-uppercase">
                        <span>or</span>
                    </span>

                    <div class="mb-xs text-center">
                        <a class="btn btn-facebook mb-md ml-xs mr-xs">Connect with <i class="fa fa-facebook"></i></a>
                        <a class="btn btn-twitter mb-md ml-xs mr-xs">Connect with <i class="fa fa-twitter"></i></a>
                    </div>-->

                    <p class="text-center">{{trans('general.dont_have_an_account_yet')}}? <a href="{{route('register')}}">{{trans('general.signup')}}!</a></p>

                </form>
            </div>
        </div>

        <p class="text-center text-muted mt-md mb-md">&copy; Copyright {{date('Y')}}. All Rights Reserved.</p>
    </div>
</section>
<!-- end: page -->

@endsection

