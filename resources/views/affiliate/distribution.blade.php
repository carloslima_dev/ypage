@extends('layouts.app')

@section('page_heading')
<i class="fa fa-sitemap"></i> {{trans('network.distribution')}}
@endsection

@section('vendorstylesheets')
<link rel="stylesheet" href="{{asset('assets/vendor/pnotify/pnotify.custom.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/primitive/css/primitives.latest.css')}}" />
@stop

@section('breadcrumb')
<div class="right-wrapper pull-right">
    <ol class="breadcrumbs">
        <li>
            <a href="index.html">
                <i class="fa fa-home"></i>
            </a>
        </li>
        <li><span>{{trans('network.distribution')}}</span></li>
    </ol>			
    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
</div>

@stop

@section('content')

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif


<!-- start: page -->
<section>
    <button class="btn btn-primary" onclick="window.location.reload()"><i class="icon icon-refresh"></i> Recarregar</button>
    <i class="legend legend-active"></i> {{trans('network.active')}}
    <i class="legend legend-inactive"></i> {{trans('network.inactive')}}
    <i class="legend legend-empty"></i> {{trans('network.empty')}}
    
    <div id="basicdiagram" style="margin-top: 0px !important; min-height: 800px !important;" /></div>
</section>

@can('network-delete')
@include('components/modal-confirm')
@endcan

@include('components/modal-user')
@endsection

@section('vendorscripts')
<script src="{{asset('assets/vendor/pnotify/pnotify.custom.js')}}"></script>
<script src="{{asset('assets/vendor/jquery-validation/jquery.validate.js')}}"></script>
<script src="{{asset('assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js')}}"></script>
<script src="{{asset('assets/vendor/jquery-maskedinput/jquery.maskedinput.js')}}"></script>
@stop
@section('javascripts')
<script>    
        var base_url = '{{url("/")}}', userId = '{{Auth::user()->id}}';
        </script>
        <script src="{{asset('assets/vendor/primitive/jquerylayout/jquery.layout-latest.min.js')}}"></script>
        <script src="{{asset('assets/vendor/primitive/js/primitives.min.js')}}"></script>        
        <script src="{{asset('assets/javascripts/network/distribution.js')}}"></script>
        <script src="{{asset('assets/javascripts/users/app.js')}}"></script>        
@stop