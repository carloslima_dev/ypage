<div id="modalUser" class="modal-block modal-block-lg mfp-hide">
    <section class="panel form-wizard" id="user">
        <div class="panel-body">
            <div class="modal-wrapper">
                <div class="modal-text">
                    @include('admin/users/partials/affiliate')
                </div>
            </div>
        </div>
        <footer class="panel-footer">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button class="btn btn-default modal-dismiss">Cancelar</button>
                </div>
            </div>
        </footer>
    </section>
</div>
