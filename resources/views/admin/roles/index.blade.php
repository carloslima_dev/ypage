@extends('layouts.app')

@section('page_heading', trans('general.management.roles'))

@section('stylesheets')
<link rel="stylesheet" href="{{asset('assets/vendor/jquery-datatables-bs3/assets/css/datatables.css')}}" />
@stop

@section('breadcrumb')
<div class="right-wrapper pull-right">
    <ol class="breadcrumbs">
        <li>
            <a href="index.html">
                <i class="fa fa-home"></i>
            </a>
        </li>
        <li><span>@yield('page_heading')</span></li>
    </ol>			
    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
</div>

@stop

@section('content')

@can('role-create')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-right">
            <a class="btn btn-default text-success" href="{{ route('roles.create') }}" title="{{ trans('general.management.new_role') }}"><i class="icon icon-plus fa-2x"></i> </a>            
        </div>
    </div>
</div>
@endcan

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif


<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title"><i class="icon icon-layers"></i></h2>        
    </header>
    <div class="panel-body">
        <div class="result-data">
            <table class="table table-bordered table-striped mb-none"  id="datatable-default" data-url="{{ route('roles.index') }}" >
                <thead>
                    <tr>
                        <th>ID#</th>
                        <th>{{trans('general.name')}}</th>
                        <th>{{trans('general.description')}}</th>
                        <th width="180px">{{trans('general.action')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($roles as $key => $role)
                    <tr>
                        <td>{{ $role->id }}</td>
                        <td>{{ $role->display_name }}</td>
                        <td>{{ $role->description }}</td>
                        <td>
                            <a class="btn text-info" href="{{ route('roles.show',$role->id) }}" title="{{trans("general.management.show_role")}}"><i class="icon icon-info"></i></a>
                            @can('role-edit')
                            <a class="btn text-warning" href="{{ route('roles.edit',$role->id) }}" title="{{trans("general.management.edit_role")}}"><i class="icon icon-note"></i></a>
                            @endcan
                            @can('role-delete')
                            <a class="btn text-danger modal-with-zoom-anim" href="#modalConfirm" onclick="deleteConfirm('{{$role->id}}', this)" title="{{trans("general.management.delete_role")}}"><i class="icon icon-trash"></i></a>                            
                            @endcan
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $roles->render() !!}
        </div>
    </div>
</section>

@include('components/modal-confirm')

@endsection

@section('vendorscripts')
<script src="{{asset('assets/vendor/jquery-datatables/media/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js')}}"></script>
<script src="{{asset('assets/vendor/jquery-datatables-bs3/assets/js/datatables.js')}}"></script>
@stop

@section('javascripts')
<script src="{{asset('assets/javascripts/tables/examples.datatables.default.js')}}"></script>
@stop