@extends('layouts.app')

@section('page_heading', trans('general.management.show_role'))

@section('breadcrumb')
<div class="right-wrapper pull-right">
    <ol class="breadcrumbs">
        <li>
            <a href="{{route('home')}}">
                <i class="icon icon-home"></i>
            </a>
        </li>
        <li>
            <a href="{{route('roles.index')}}">
                {{trans('general.management.roles')}}
            </a>
        </li>        
        <li><span>@yield('page_heading')</span></li>
    </ol>			
    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
</div>

@stop

@section('content')

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title"><i class="icon icon-layers"></i></h2>
    </header>
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>{{trans('general.name')}}:</strong>
                    {{ $role->display_name }}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>{{trans('general.description')}}:</strong>
                    {{ $role->description }}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>{{trans('general.permissions')}}:</strong>
                    @if(!empty($rolePermissions))
                    @foreach($rolePermissions as $v)
                    <label class="label label-success">{{ $v->display_name }}</label>
                    @endforeach
                    @endif
                </div>
            </div>
        </div>

    </div>
</section>
@endsection
