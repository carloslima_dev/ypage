@extends('layouts.app')

@section('page_heading', trans('general.management.show_user'))

@section('breadcrumb')
<div class="right-wrapper pull-right">
    <ol class="breadcrumbs">
        <li>
            <a href="{{route('home')}}">
                <i class="icon icon-home"></i>
            </a>
        </li>
        <li>
            <a href="{{route('users.index')}}">
                {{trans('general.management.users')}}
            </a>
        </li>        
        <li><span>@yield('page_heading')</span></li>
    </ol>			
    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
</div>

@stop

@section('content')

<!-- start: page -->
<section class="panel">
    <div class="panel-body">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>{{trans('general.name')}}:</strong>
                    {{ $user->name }}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>{{trans('general.email')}}</strong>
                    {{ $user->email }}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>{{trans('general.roles')}}:</strong>
                    @if(!empty($user->getRoleNames()))
                    @foreach($user->getRoleNames() as $v)
                    <label class="badge badge-success">{{ $v }}</label>
                    @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
@endsection