@extends('layouts.app')

@section('page_heading', trans('general.management.users'))

@section('breadcrumb')
<div class="right-wrapper pull-right">
    <ol class="breadcrumbs">
        <li>
            <a href="index.html">
                <i class="fa fa-home"></i>
            </a>
        </li>
        <li><span>@yield('page_heading')</span></li>
    </ol>			
    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
</div>

@stop

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-right">
            <a class="btn btn-default text-success" href="{{ route('users.create') }}" title="{{ trans('general.management.new_user') }}"><i class="icon icon-user-follow fa-2x"></i> </a>
        </div>
    </div>
</div>


@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif


<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title"><i class="icon icon-people"></i></h2>        
    </header>
    <div class="panel-body">
        <div class="result-data hidden">
            <table class="table table-bordered table-striped mb-none"  id="table-ajax" data-url="{{ route('users.index') }}" data-model="users">
                <thead>
                    <tr>
                        <th>#ID</th>
                        <th>{{ trans('general.name') }}</th>
                        <th>{{ trans('general.reference') }}</th>
                        <th>{{ trans('general.email') }}</th>
                        <th width="150px">{{ trans('general.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div>
                        {{trans('general.showing')}} 
                        <span id="table-info-from"></span> 
                        {{trans('general.to')}} 
                        <span id="table-info-to"></span> 
                        {{trans('general.of')}} 
                        <span id="table-info-total"></span>
                        {{trans('general.entries')}}
                    </div>                    
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="pull-right">
                        <ul class="pagination">
                        </ul>
                    </div>
                </div>
            </div>        
        </div>
        <div class="empty hidden" id="empty">
            <h4>{{trans('general.empty_data')}}</h4>
        </div>
    </div>
</section>

@endsection

@section('javascripts')
<script>
    var titleShow = '{{trans("general.management.show_user")}}',
        titleEdit = '{{trans("general.management.edit_user")}}',
        titleDelete = '{{trans("general.management.delete_user")}}'
        $token = '{{ csrf_token() }}';
</script>                

<script src="{{asset('assets/javascripts/tables/tables.ajax.js')}}"></script>
@stop