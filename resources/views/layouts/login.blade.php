<!doctype html>
<html class="fixed" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @include('layouts/head')
    <body class="login">
        @yield('content')
    @include('layouts/footer')
    </body>
</html>