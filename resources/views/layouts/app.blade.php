<?php
    setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
    date_default_timezone_set('America/Sao_Paulo');
    $hoje = strftime('%A, %d de %B de %Y', strtotime('today'));
?>
<!doctype html>
<html class="fixed sidebar-left-collapsed" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @include('layouts/head')
    <body>
        <section class="body">
            @include('layouts/header')
            <div class="inner-wrapper">
                @if(!Auth::guest())
                @include('layouts/sidebar')                
                @endif
                <section role="main" class="content-body pb-none">
                    @if(!Auth::guest())
                    <header class="page-header">
                        <h2>@yield('page_heading')</h2>
                        @yield('breadcrumb')
                    </header>
                    @endif
                    <!-- start: page -->
                    @if(session()->has('success'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <strong>Sucesso!</strong><br>{{session('success')}}<br>
                    </div>
                    @endif

                    @yield('content')
                    <!-- end: page -->
                </section>
            </div>
            <aside id="sidebar-right" class="sidebar-right">
                <div class="nano">
                    <div class="nano-content">
                        <a href="#" class="mobile-close visible-xs">
                            Collapse <i class="fa fa-chevron-right"></i>
                        </a>

                        <div class="sidebar-right-wrapper">

                            <div class="sidebar-widget widget-calendar">
                                <h6>Data e hora</h6>
                                <div data-plugin-datepicker data-plugin-skin="dark" ></div>
                                <ul>
                                    <li>
                                        <time datetime="{{date('Y-m-d')}}T00:00+00:00">{{$hoje}}</time>
                                        <span class="icons icon-clock"></span> <span id="time"></span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </aside>

        </section>
        @include('layouts/footer')
    </body>
</html>