<?php
return [
    'users' => 'Users',
    'management' => [
        'users'         => 'Users Management',
        'new_user'      => 'Create new user',
        'show_user'     => 'Show user',
        'edit_user'     => 'Edit user',
        'delete_user'   => 'Delete user'
    ],
    'name' => 'Name',
    'email' => 'Email',
    'action' => 'Action'
];