<?php
return [
    'network' => 'Rede',
    'network-list' => 'Listar Rede',
    'laterality' => 'Lateralidade',
    'laterality-list' => 'Listar Lateralidade',
    'distribution' => 'Derramamento',
    'active' => 'Ativo',
    'inactive' => 'Inativo',
    'empty' => 'Vazio',
];