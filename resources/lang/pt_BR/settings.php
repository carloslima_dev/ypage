<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    'settings' => [
        'settings' => 'Configurações',
        'application_name' => 'Nome do Aplicativo',
        'company_address' => 'Endereço da Empresa',
        'company_cnpj' => 'CNPJ da Empresa',
        'company_slogan' => 'Mercado online para compra ou venda secundária de ingressos para eventos em todo o mundo. Os preços são definidos pelos vendedores e podem ser iguais ou superiores ao valor nominal.',
        'logo' => 'Logo',
        'fav' => 'Favicon',
        'description_recommended_logo_resolution' => 'A resolução recomendada do logotipo é de 216 x 38 pixels.',
        'description_recommended_fav_resolution' => 'A resolução recomendada do favicon é de 40 x 40 pixels.',
        'default_currency' => 'Moeda padrão',
        'default_timezone' => 'Fuso horário padrão',
        'default_language' => 'Idioma padrão',
        'support_number' => 'Telefone de suporte',
        'support_email_id' => 'Email de suporte',
        'commision_percentage' => 'Comissão (%)',
        'seller_fee' => 'Taxa do vendedor (%)',
        'tax' => 'Imposto (%)',
        'buyer_fee' => 'Taxa do comprador (%)',
        'cancellation_fine_fee' => 'Multa de cancelamento',
        'mail_driver' => 'Driver do e-mail de saída',
        'mail_host' => 'Servidor do e-mail de saída',
        'mail_port' => 'Porta de saída',
        'mail_username' => 'Usuário do e-mail de saída',
        'mail_password' => 'Senha do e-mail de saída',
        'mail_encryption' => 'Criptografia do email de saída',
        'mail_from_name' => 'Nome do remetente de email',
        'mail_from_address' => 'E-mail do remetende',        
        'all_rights_reserved' => 'Todos os direitos reservados',
        'pagseguro_email'   => 'Email PagSeguro',
        'pagseguro_token'   => 'Token'        
    ],
    'dashboard' => [
        'total_orders' => 'Total de encomendas',
        'total_revenue' => 'Rendimento total',
        'total_visitors' => 'Total de usuários',
        'today' => 'Hoje',
        'total_events' => 'Total Eventos',
        'seller_commission' => 'Comissão do vendedor',
        'tickets_posted' => 'Ingressos postados',
        'cancelled_orders' => 'Pedidos cancelados',
        'recent_orders' => 'pedidos recentes',
        'un_published' => 'Não publicado',
        'published' => 'Publicados',
        'view_users' => 'Ver Usuários',
        'recent_events' => 'Eventos recentes',
        'sellers' => 'Vendedores',
        'buyers' => 'Compradores',
        'order_id' => 'ID do Pedido',
        'order_placed' => 'Pedido feito',
        'order_date' => 'Data do pedido',
        'event_name' => 'Nome do evento',
        'no_of_tickets' => 'Nº de ingressos',
        'ticket_amount' => 'Quantidade',
        'payment_method' => 'Método de pagamento',
        'buyer' => 'Comprador',
        'seller' => 'Vendedor',
        'status' => 'Status',
        'seller_status' => 'Últimos ingressos postados',
        'seller_id' => 'ID do vendedor',
        'seller_name' => 'Nome do vendedor',
        'tickets_posted' => 'Ingressos postados',
        'tickets_sold' => 'Ingressos vendidos',
        'tickets_amount' => 'Montante dos ingressos',
        'commission' => 'Comissão',
        'date' => 'Data',
        'time' => 'Hora',
        'event_name' => 'Nome do Evento',
        'event_description' => 'Descrição do Evento',
        'event_date' => 'Data do Evento',
        'states' => 'Estados'
    ],
    'profile' => [
      'profile_updated_successfully' => 'Perfil alterado com sucesso'
    ],
    'slot' => [
        'collection_slot' => 'Faixa de coleta',
        'slots' => 'Faixas',
        'add_delivery_slots' => 'Adicionar faixas de entrega',
        'filter_by_slot_name' => 'Filtrar por faixa de Coleta',
        'filter_by_slot' => 'Filtrar por Faixa',
        'filter_by_to' => 'Filtrar por',
        'edit_delivery_slots' => 'Editar faixa de entrega',
        'slot_name' => 'Nome do faixa',
        'delivery_slots' => 'Faixa de Entrega',
        'from_slot' => 'A partir da faixa',
        'to_slot' => 'Para faixa',
    ],
    'transactionmode' => [
        'payment_method' => 'Método de pagamento',
        'filter_by_paymentmethod' => 'Filtrar por método de pagamento',
        'add_payment_type' => 'Adicionar método de pagamento',
        'payment_type' => 'Método de pagamento',
        'add_payment' => 'Adicionar método de pagamento',
        'edit_payment' => 'Editar método de pagamento',
    ],
    'deliverytype' => [
        'add_product_type_btn' => 'Adicionar tipo de pacote',
        'parent_product_type' => 'Tipo de pacote',
        'child_product_type' => 'Nome do pacote',
        'filter_by_parent' => 'Filtrar por Tipo de pacote',
        'filter_by_child' => 'Filtrar por nome do pacote',
        'product_type' => 'Tipo de Pacote',
        'add_product_type' => 'Adicionar tipo de pacote',
        'edit_product_type' => 'Editar tipo de pacote',
    ],
    'parceltype' => [
        'packages_type' => 'Tamanho do pacote',
        'weight' => 'Peso',
        'width' => 'Largura',
        'height' => 'Altura',
        'filter_by_package_type' => 'Filtrar por pacote Tamanho Nome',
        'filter_by_weight' => 'Filtrar por peso do pacote',
        'filter_by_width' => 'Filtrar por largura do pacote',
        'filter_by_height' => 'Filtrar por altura do pacote',
        'add_package_type' => 'Adicionar tamanho do pacote',
        'edit_package_type' => 'Editar tamanho do pacote',
        'package_name' => 'Tamanho do pacote Nome',
    ],
    'shipmenttype' => [
        'shipment_type' => 'Tipo de entrega',
        'filter_by_shipment_type' => 'Filtrar por tipo de entrega',
        'add_shipment_type' => 'Adicionar tipo de entrega',
        'edit_shipment_type' => 'Editar tipo de entrega'
    ],
    'shipment' => [
        'manage_shipments' => 'Gerenciar remessas',
        'shipment_heading' => 'Envios',
        'shipment_id' => 'ID de Envio',
        'order_type' => 'Tipo de Pedido',
        'username' => 'Nome de usuário',
        'user_type' => 'Tipo de usuário',
        'parcel_type' => 'Tipo de parcela',
        'ipin_status' => 'IPIN Status',
        'payment_mode' => 'Modo de pagamento',
        'package_title' => 'Título do Pacote',
        'shipment_name' => 'Nome do Envio',
        'shipment_price' => 'Preço de Envio',
        'filter_by_shipment_name' => 'Filtrar por nome de envio',
        'filter_by_order_type' => 'Filtrar por tipo de pedido',
        'filter_by_username' => 'Filtrar por nome de usuário',
        'filter_by_user_type' => 'Filtrar por tipo de usuário',
        'filter_by_parcel_type' => 'Filtrar por tipo de parcela',
        'filter_by_ipin_status' => 'Filtrar por IPIN Status',
        'filter_by_payment_mode' => 'Filtrar por modo de pagamento',
        'filter_by_package_title' => 'Filtrar por título do pacote',
        'filter_by_shipment_price' => 'Filtrar por preço de envio',
        'single' => 'solteiro',
        'multiple' => 'Múltiplo',
        'docs' => 'Docs',
        'nondocs' => 'NonDocs',
    ],
    'order' => [
        'manage_orders' => 'Gerenciar Pedidos',
        'order_heading' => 'Pedidos',
        'order_id' => 'ID do Pedido',
        'shipment_id' => 'ID de Envio',
        'username' => 'Nome de usuário',
        'sellername' => 'Nome do vendedor',
        'buyername' => 'Nome do comprador',
        'event_name' => 'Nome do evento',
        'no_of_ticket' => 'Quant',
        'ticket_type' => 'Tipo',
        'price' => 'Preço',
        'tax' => 'Imposto',
        'publish' => 'Publicar',
        'total_amount' => 'Valor total',
        'order_status' => 'Status do pedido',
        'country' => 'País',
        'language' => 'Línguagem',
        'currency' => 'Moeda',
        'payment_type' => 'Tipo de pagamento',
        'payment_status' => 'Status do pagamento',
        'status' => 'Status',
        'payment_mode' => 'Modo de pagamento',
        'shipment_name' => 'Nome do Envio',
        'shipment_price' => 'Preço de Envio',
        'created_at' => 'Criado em',
        'updated_at' => 'Modificado em',
        'filter_by_shipment_name' => 'Filtrar por nome de remessa',
        'filter_by_order_ID' => 'Filtrar por ID do pedido',
        'filter_by_username' => 'Filtrar por nome de usuário',
        'filter_by_payment_type' => 'Filtrar por tipo de pagamento',
        'filter_by_payment_status' => 'Filtrar por estado de pagamento',
        'filter_by_payment_mode' => 'Filtrar por modo de pagamento',
        'filter_by_shipment_price' => 'Filtrar por preço de envio',
        'filter_by_event_name' => 'Filtrar por nome do evento',
        'filter_by_ticket_type' => 'Filtrar por tipo de ingresso',
        'filter_by_ticket_price' => 'Filtrar por Preço do Ingresso',
        'filter_by_status' => 'Filtrar por status',
        'filter_by_date' => 'Filtrar por data de criação'
    ],
    'reports' => [
        'report_heading' => 'Relatórios',
        'order_id' => 'ID do Pedido',
        'shipment_id' => 'ID do envio',
        'username' => 'Nome do Usuário',
        'event_name' => 'Nome do Evento',
        'no_of_ticket' => 'Nº de Bilhete',
        'ticket_type' => 'Tipo de Ingresso',
        'price' => 'Preço',
        'tax' => 'Inposto',
        'total_amount' => 'Valor Total',
        'order_status' => 'Status do Pedido',
        'country' => 'País',
        'language' => 'Linguagemnguagem',
        'currency' => 'Moeda',
        'payment_type' => 'Tipo de Pagamento',
        'payment_status' => 'Status do Pagamento',
        'status' => 'Status',
        'payment_mode' => 'Modo de pagamento',
        'shipment_name' => 'Nome da Envio',
        'shipment_price' => 'Preço do Envio',
        'export' => 'Exportar',
        'created_at' => 'Criado em',
        'filter_by_shipment_name' => 'Filtrar por nome do envio',
        'filter_by_order_ID' => 'Filtrar por ID do pedido',
        'filter_by_username' => 'Filtrar por nome de usuário',
        'filter_by_payment_type' => 'Filtrar por tipo de pagamento',
        'filter_by_payment_mode' => 'Filtrar por modo de pagamento',
        'filter_by_shipment_price' => 'Filtrar por preço de envio',
        'filter_by_event_name' => 'Filtrar por nome do evento',
        'filter_by_ticket_type' => 'Filtrar por tipo de ingresso',
        'filter_by_ticket_price' => 'Filtrar por Preço do ingresso',
        'filter_by_status' => 'Filtrar por status',
        'filter_by_date' => 'Filtrar por data de criação'
    ],
    'sales' => [
        'sales_heading' => 'Gerenciar Vendas',
        'order_id' => 'ID do Pedido',
        'shipment_id' => 'ID do Envio',
        'username' => 'Nome do Usuário',
        'event_name' => 'Nome do Evento',
        'no_of_ticket' => 'Não do ingresso',
        'ticket_type' => 'Tipo de ingresso',
        'price' => 'Preço',
        'tax' => 'Imposto',
        'total_amount' => 'Valor total',
        'order_status' => 'Status do pedido',
        'country' => 'País',
        'language' => 'Idioma',
        'currency' => 'Moeda',
        'payment_type' => 'Tipo de pagamento',
        'payment_status' => 'Status do pagamento',
        'status' => 'Status',
        'payment_mode' => 'Modo de pagamento',
        'shipment_name' => 'Nome do embarque',
        'shipment_price' => 'Preço de envio',
        'export' => 'Exportar',
        'created_at' => 'Criado em',
        'filter_by_shipment_name' => 'Filtrar por Nome do Embarque',
        'filter_by_order_ID' => 'Filtrar por ID de pedido',
        'filter_by_username' => 'Filtrar por nome de usuário',
        'filter_by_payment_type' => 'Filtrar por tipo de pagamento',
        'filter_by_payment_mode' => 'Filtrar por modo de pagamento',
        'filter_by_shipment_price' => 'Filtrar por preço de envio',
        'filter_by_event_name' => 'Filtrar por Nome do Evento',
        'filter_by_ticket_type' => 'Filtrar por Tipo de ingresso',
        'filter_by_ticket_price' => 'Filtrar por preço do ingresso',
        'filter_by_status' => 'Filtrar por Status',
        'filter_by_date' => 'Filtrar por data de criação'
    ],
    'bank' => [
        'add_bank' => 'Adicionar Banco',
        'banks' => 'Bancos',
        'edit_bank' => 'Editar Banco',
    ],
    'places' => [
        'filter_by_zipcode' => 'Filtrar por CEP',
        'filter_by_country_name' => 'Filtrar por País',
        'filter_by_state_name' => 'Filtrar por Estado',
        'filter_by_city_name' => 'Filtrar por Cidade',
        'filter_by_district_name' => 'Filtrar por Bairro/Setor',
        'filter_by_name' => 'Filtrar por Nome',
        'filter_by_short_name' => 'Filtrar por Nome Curto',
        'filter_by_id' => 'Filtrar por ID',
    ]
    
];

