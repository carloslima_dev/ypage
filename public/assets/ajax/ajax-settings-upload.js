
var Upload = function (file) {
    this.file = file;
};

var uploaded, imgSettingUploaded, progress_bar_id;

Upload.prototype.getType = function() {
    return this.file.type;
};
Upload.prototype.getSize = function() {
    return this.file.size;
};
Upload.prototype.getName = function() {
    return this.file.name;
};
Upload.prototype.doUpload = function () {      
    $(progress_bar_id).removeClass('hidden');
    $(progress_bar_id + " .progress-bar").css("width", "0%");
    $(progress_bar_id + " .status").text("0%");
    
    var that = this;
    var formData = new FormData();

    formData.append("file", that.file);
    formData.append("upload_file", true);
    formData.append("setting_name", progress_bar_id.replace('#progress-wrp-', ''));

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $token
        }
    });

    $.ajax({
        type: "POST",
        url: $url,
        xhr: function () {
            var myXhr = $.ajaxSettings.xhr();
            if (myXhr.upload) {
                myXhr.upload.addEventListener('progress', that.progressHandling, false);
            }
            return myXhr;
        },
        success: function (data) {
            $(uploaded).val(data.filename);
            $(imgSettingUploaded).attr('src', data.filename);
        },
        error: function (error) {
            $(progress_bar_id).addClass('hidden');
            new PNotify({
                title: titleError + '!',
                text: textError + '. ' + error.responseJSON.message,
                type: 'error'
            });
            $('.img-uploaded img').attr('src', imgError);
        },
        async: true,
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        timeout: 60000
    });
};

Upload.prototype.progressHandling = function (event) {
    var percent = 0;
    var position = event.loaded || event.position;
    var total = event.total;
    if (event.lengthComputable) {
        percent = Math.ceil(position / total * 100);
    }
    // update progressbars classes so it fits your code
    $(progress_bar_id + " .progress-bar").css("width", +percent + "%");
    $(progress_bar_id + " .status").text(percent + "%");    
};

$(".upload").on("click", function(e) {    
    var c = '.' + $(e.target).attr('data-name');
    $(c).trigger('click');
});

$(".general_settings__logo-file").on("change", function (e) {
    var upload = new Upload($(this)[0].files[0]);
    uploaded = ".general_settings__logo-fileuploaded";
    imgSettingUploaded = '.uploaded-general_settings__logo img';
    progress_bar_id = "#progress-wrp-general_settings__logo";
    // maby check size or type here with upload.getSize() and upload.getType()

    // execute upload
    upload.doUpload();
});

$(".general_settings__fav-file").on("change", function (e) {
    var upload = new Upload($(this)[0].files[0]);
    uploaded = ".general_settings__fav-fileuploaded";
    imgSettingUploaded = '.uploaded-general_settings__fav img';
    progress_bar_id = "#progress-wrp-general_settings__fav";
    // maby check size or type here with upload.getSize() and upload.getType()

    // execute upload
    upload.doUpload();
});