            var myVar = null;
            $('.sidebar-right-toggle').on('click', function() {
                
                if ($('html').hasClass('sidebar-right-opened')) {
                    myVar = setInterval(myTimer ,1000);
                } else {
                    clearInterval(myVar);
                    console.log('nao-tem')
                }
            });            
            function myTimer() {
                var d = new Date(), displayDate;
                if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
                    displayDate = d.toLocaleTimeString('pt-BR');
                } else {
                    displayDate = d.toLocaleTimeString('pt-BR', {timeZone: 'America/Sao_Paulo'});
                }
                document.getElementById("time").innerHTML = displayDate;
            }           

$.extend(theme.PluginDatePicker.defaults, {
    format: "dd/mm/yyyy",
    dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
    dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
    monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
    nextText: 'Próximo',
    prevText: 'Anterior'    
});