<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserPhonesChangePhoneNumberAndContactNameToNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_phones', function (Blueprint $table) {
            $table->string('phone_number', 25)->nullable()->change();
                    
            $table->string('contact_name', 255)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_phones', function (Blueprint $table) {
            //
        });
    }
}
