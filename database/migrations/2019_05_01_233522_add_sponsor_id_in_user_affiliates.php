<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSponsorIdInUserAffiliates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_affiliates', function (Blueprint $table) {
            $table->bigInteger('sponsor_id')->unsigned()->after('parent_id')->nullable();
            
            $table->foreign('sponsor_id')->references('id')->on('users')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_affiliates', function (Blueprint $table) {
            //
        });
    }
}
