<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('setting_category_id', false, true);
            $table->string('setting_name');
            $table->string('setting_value');
            $table->string('display_name');
            $table->string('class');
            $table->string('type');
            $table->string('option');
            $table->string('description');
            $table->integer('order');
            $table->tinyInteger('is_active')->default(1);
            $table->timestamps();
            
            $table->foreign('setting_category_id')->references('id')->on('setting_categories')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
