<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    public function settingCategory()
    {
        return $this->belongsTo(SettingCategory::class);
    }
    
  /**
   * Method used to get the option list based on the settings class
   *
   * @return array
   */
  public function getOption() {
    if ($this->class && ($classInstance = $this->getClassInstance ())) {
      return $classInstance->getOptionList ();
    } else {
      return explode ( ",", $this->option );
    }
  }
  
  /**
   * Method used to create instance for the class updated in settings
   *
   * @return Object
   */
  public function getClassInstance() {
    $classInstance = false;
    $status = false;
  
    if (class_exists ( $this->class )) {
      $classInstance = new $this->class ();
    }
  
    return ($classInstance instanceof ConfigurableModel) ? $classInstance : $status;
  }
    
}
