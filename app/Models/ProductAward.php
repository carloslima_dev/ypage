<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductAward extends Model
{
    protected $fillable = [
        'product_id',
        'value',
        'calc_type',
        'type',
        'level',
        'is_active'
    ];

}
