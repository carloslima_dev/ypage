<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name',
        'detail',
        'photo',
        'price',
        'is_active'
    ];
    
    public function bonus_d()
    {
        return $this->hasMany(ProductAward::class)->where('type', 'd');
    }
    
    public function bonus_p()
    {
        return $this->hasMany(ProductAward::class)->where('type', 'p');
    }
    
}
