<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SettingCategory extends Model
{
    public function settings()
    {
        return $this->hasMany(Setting::class);
    }
}
