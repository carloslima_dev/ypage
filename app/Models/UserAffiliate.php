<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class UserAffiliate extends Model
{
    protected $fillable = [
        'parent_id',
        'sponsor_id',
        'position_l',
        'user_id',
        'data_nascimento',
        'sexo',
        'rg',
        'orgao_emissor',
        'cpf'
    ];

    public function setDataNascimentoAttribute($value)
    {
        $this->attributes['data_nascimento'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
    }    
    
    public function getDataNascimentoAttribute($value)
    {
        $date = Carbon::parse($value);
        return $date->format('d/m/Y');
    }    
    
    public function parentUser()
    {
        return $this->belongsTo(\App\User::class, 'parent_id', 'id');
    }
    
    public function user()
    {
        return $this->belongsTo(\App\User::class, 'user_id', 'id');
    }

    public function children()
    {
        return $this->hasMany(UserAffiliate::class, 'parent_id', 'user_id')->with('children.user.userPhone');
    }
    
    public function sponsored()
    {
        return $this->hasMany(UserAffiliate::class, 'sponsor_id', 'user_id')->with('sponsored.user.userPhone');
    }

}
