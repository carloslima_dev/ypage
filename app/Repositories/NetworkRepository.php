<?php

namespace App\Repositories;


use App\User;
use App\Models\UserAffiliate;
use App\Models\UserAddress;
use App\Models\UserBank;
use App\Models\UserPhone;

use DB;
use Hash;
use Auth;
use Illuminate\Validation\Rule;

/**
 * Description of NetworkRepository
 *
 * @author fernando
 */
class NetworkRepository extends BaseRepository
{
    
    private $userAffiliate;
    public $arrayUsers = [];
    
    public function __construct(UserAffiliate $userAffiliate) 
    {
        $this->userAffiliate = $userAffiliate;
    }
        
    public function store($request)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => 'required|same:confirm-password',
            'roles' => ['required', Rule::notIn('admin')],
            'userAffiliate.data_nascimento' => 'required',
            'userAffiliate.sexo' => 'required',
            'userAffiliate.rg' => ['required', Rule::unique('user_affiliates', 'rg')],
            'userAffiliate.orgao_emissor' => 'required',
            'userAffiliate.cpf' => ['required', Rule::unique('user_affiliates', 'cpf')],
            'userBank.bank_id' =>  'required',
            'userBank.agency' =>  'required',
            'userBank.account' =>  'required',
            'userBank.account_type' =>  'required',
            'userBank.holder_document' =>  'required',
            'userBank.holder_name' =>  'required',            
        ]);
        
        try {
            $input = $request->all();
            $input['password'] = Hash::make($input['password']);


            $user = User::create($input);
            $user->assignRole($request->input('roles'));


            if ($user) {

                //create or update userAffiliate
                $input['userAffiliate']['user_id'] = $user->id;
                $input['userAffiliate']['sponsor_id'] = isset($input['userAffiliate']['sponsor_id']) ? $input['userAffiliate']['sponsor_id'] : Auth::user()->id;
                
                UserAffiliate::create($input['userAffiliate']);

                //create or update userAddress
                if (!empty($input['userAddress'])) {
                    foreach ($input['userAddress'] as $address) {
                        $address['user_id'] = $user->id;
                        UserAddress::create($address);
                    }
                }

                //create or update userPhones
                if (!empty($input['userPhone'])) {
                    foreach ($input['userPhone'] as $phone) {
                        $phone['user_id'] = $user->id;
                        UserPhone::create($phone);
                    }
                }

                $input['userBank']['user_id'] = $user->id;

                UserBank::create($input['userBank']);
            }

            return ['success' => 'Usuário cadastrado com sucesso'];
            
        } catch (Exception $ex) {
            return ['error' => $ex];
        }
        
    }
    public function sponsor($userId)
    {
        return $this->userAffiliate->with(['user.userIndicated.user.userPhone'])->where('user_id', $userId)->first();
    }
    
    public function allChildren($userId)
    {
        return $this->userAffiliate->with(['user.userPhone', 'children.user.userPhone'])->where('user_id', $userId)->first();
    }

    public function allSponsored($userId)
    {
        return $this->userAffiliate->with(['user.userPhone', 'sponsored.user.userPhone'])->where('user_id', $userId)->first();
    }
    
    public function arrayUser($userAffiliate, $users = [], $type = 'distribution') 
    {

        if (empty($userAffiliate)) {
            return null;
        }
        $phoneNumber = isset($userAffiliate['user']) ? $this->getThePhone($userAffiliate) : '';
        $name = isset($userAffiliate['user']) ? $userAffiliate['user']['name'] : '';
        $email = isset($userAffiliate['user']) ? $userAffiliate['user']['email'] : '';
        
        $array = [
            'id' => $userAffiliate['user_id'] ?? '',
            'parent_id' => $userAffiliate['parent_id'],
            'sponsor_id' => $userAffiliate['sponsor_id'],
            'position_l' => $userAffiliate['position_l'] ?? 0,
            'name' => $name,
            'email' => $email,
            'is_active' => $userAffiliate['is_active'] ?? '',
            'phone' => $phoneNumber,
            'thumb' => $this->getThumb($userAffiliate),
            'haveChildren' => !empty($userAffiliate['sponsored'])
        ];

        array_push($this->arrayUsers, $array);

        if ($type = 'laterality' && !empty($userAffiliate['sponsored'])) {
            $this->recursive($userAffiliate['sponsored'], $users);
        }

        if ($type = 'distribution' && !empty($userAffiliate['children'])) {
            $this->recursive($userAffiliate['children'], $users);
        }
        
    }
    
    public function getThePhone($userAffiliate)
    {
        $phoneNumber = '';
        foreach ($userAffiliate['user']['user_phone'] as $phone) {
            if (!empty($phone['phone_number'] && $phone['phone_number'] !== '')) {

                if ($phone['phone_type_id'] === 1) {
                    $phoneNumber = $phone['phone_number'];
                }

                if ($phone['phone_type_id'] === 2) {
                    $phoneNumber = $phone['phone_number'];
                }

                if ($phone['phone_type_id'] === 3) {
                    $phoneNumber = $phone['phone_number'];
                }

                if ($phone['phone_type_id'] === 4) {
                    $phoneNumber = $phone['phone_number'];
                }
            }
        }
        
        return $phoneNumber;
    }
    
    public function recursive($children, $users)
    {
        foreach ($children as $user) {      
            $this->arrayUser($user, $users);
        }
    }
    
    private function getThumb($user)
    {
        if (empty($user->photo)) {
            if ($user['sexo'] === 'F') {
                return asset('assets/images/female.png');
            } else {
                return asset('assets/images/male.png');
            }            
        }
        return asset($user['photo']);
    }
    
    public function generateArray3x3($array)
    {
        if (isset($array['children']) && count($array['children']) < 3) {

            for ($i = count($array['children']); $i < 3; $i++) {                
                $arrPosition = $this->getUniquePosition($array['children']);
                $array['children'][$i]['user_id'] = Hash::make($array['user_id']);
                $array['children'][$i]['parent_id'] = $array['user_id'];
                $array['children'][$i]['sponsor_id'] = $array['sponsor_id'];
                $array['children'][$i]['position_l'] = $arrPosition[0];
                $array['children'][$i]['name'] = '';
                $array['children'][$i]['email'] = '';
                $array['children'][$i]['is_active'] = null;
                $array['children'][$i]['phone'] = '';
                $array['children'][$i]['sexo'] = '';
                $array['children'][$i]['thumb'] = $this->getThumb($array);
                
            }
                        
            foreach ($array['children'] as $k => $children) {
                if (isset($children['children']) && count($children['children']) < 3) {
                    $array['children'][$k] = $this->generateArray3x3($children);
                }
            }
        }
        return $array;
    }
    
    private function getUniquePosition($arr)
    {
        $numeros = [];
        if (!empty($arr)) {
            foreach ($arr as $a) {
                if( isset($a['position_l'])) $numeros[] = $a['position_l'];
            }
        }        
        $numeros2 = [];
        $total 	= 5;
        $min 	= 1;
        $max 	= 5;

        while ( count( $numeros2 = array_unique( $numeros2 ) ) < $total ) $numeros2[] = rand( $min , $max );

        $arrDiff = array_diff($numeros2, $numeros);
        sort($arrDiff);
        
        $arrPosition = [];
        foreach ( $arrDiff as $diff ) {
            $arrPosition[] = $diff;
        }
        return  $arrPosition;
    }
    
    function isChildren($userId, $children = [], $nivel = 0) 
    {        
        $return = false;
        if (empty($children)) {
            $children = $this->allChildren(Auth::user()->id)->toArray();
        }

        if (!empty($children['children'])) {

            foreach ($children['children'] as $user) {
        
                if ($user['user_id'] == $userId) {
                    $return = true;
                    break;
                }                
                if (!empty($user['children'])) {                    
                    $return = $this->isChildren($userId, $user, $nivel++);
                }
                continue;
            }                
        } else {
            
            if (($nivel == 0 && $children[0]['parent_id'] == Auth::user()->id) || ($nivel > 0 && $children[0]['user_id'] == $userId) ) {
                    $return = true;
            }
        }     
        return $return;
    }

}
