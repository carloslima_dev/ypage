<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    
    
    protected $guard_name = 'web';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    /**
     * Return relationship one to one for the affiliate data
     * @return type
     */
    public function userAffiliate()
    {
        return $this->hasOne(Models\UserAffiliate::class);
    }
    
    public function userIndicated()
    {
        return $this->hasMany(Models\UserAffiliate::class, 'parent_id', 'id');
    }
    
    public function userAddress()
    {
        return $this->hasMany(Models\UserAddress::class);
    }
    
    public function userPhone()
    {
        return $this->hasMany(Models\UserPhone::class);
    }

    public function userBank()
    {
        return $this->hasOne(Models\UserBank::class);
    }
    
    
}

