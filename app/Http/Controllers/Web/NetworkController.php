<?php

namespace App\Http\Controllers\Web;

use App\Repositories\NetworkRepository;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;

use Auth;
use App\Models\Product;

class NetworkController extends Controller
{
    private $networkRepository;
    
    public function __construct(NetworkRepository $networkRepository) {
        $this->networkRepository = $networkRepository;
        
         $this->middleware('permission:network-list');
         $this->middleware('permission:network-create', ['only' => ['create','store']]);
         $this->middleware('permission:network-edit', ['only' => ['edit','update']]); 
         $this->middleware('permission:network-delete', ['only' => ['destroy']]);
        
    }
    
    public function sponsor()
    {
        $user = $this->networkRepository->sponsor(Auth::user()->id);
        $userIndicated = $user->user->userIndicated;

        return view('affiliate.sponsor', compact('user', 'userIndicated'));
    }
    
    public function laterality(Request $request)
    {
        $parentUsers = [Auth::user()->id => Auth::user()->name];
        if (!empty($request->input('user'))) {
            $userId = $request->input('user');
            if (!checkIfIsAdmin()) { 
                if (!$this->networkRepository->isChildren($userId)) {
                    $userId = Auth::user()->id;
                }
            }
                       
            $network = $this->networkRepository->allSponsored($userId)->toArray();

            $users = [];
            $this->networkRepository->arrayUser($network, $users, 'laterality');
        
            $allChildren = $this->networkRepository->arrayUsers;

            return $allChildren;
        }
        $genders = ['M' => 'Masculino', 'F' => 'Feminino'];
        $states = states();
        $banks = banks()->pluck('title', 'id');
        
        $accountTypes = ['Conta Corrente' => 'Conta Corrente', 'Conta Poupança' => 'Conta Poupança'];
        return view('affiliate.laterality',compact('user','roles','parentUsers', 'genders', 'states', 'banks', 'accountTypes'));        
        
    }

    public function distribution(Request $request)
    {
        $parentUsers = [Auth::user()->id => Auth::user()->name];
        if (!empty($request->input('user'))) {
            $userId = $request->input('user');
            if (!checkIfIsAdmin()) { 
                if (!$this->networkRepository->isChildren($userId)) {
                    $userId = Auth::user()->id;
                }
            }
            
            $network = $this->networkRepository->allChildren($userId)->toArray();
            
            if (count($network['children']) < 3) {
                $network = $this->networkRepository->generateArray3x3($network);
            }

            $users = [];
            $this->networkRepository->arrayUser($network, $users);
        
            $allChildren = $this->networkRepository->arrayUsers;

            return $allChildren;
        }
        $genders = ['M' => 'Masculino', 'F' => 'Feminino'];
        $states = states();
        $banks = banks()->pluck('title', 'id');
        
        $accountTypes = ['Conta Corrente' => 'Conta Corrente', 'Conta Poupança' => 'Conta Poupança'];
        return view('affiliate.distribution',compact('user','roles','parentUsers', 'genders', 'states', 'banks', 'accountTypes'));        
        
    }
    
    public function create()
    {
        $parentUsers = Auth::user()->id;
        $showRoles = false;
        $roles = \App\User::find(Auth::user()->id)->getRoleNames();
        foreach ($roles as $role) {
            if ($role == 'admin') {
                $parentUsers = parents();
                $showRoles = true;
                break;
            }
        }        
        $roles = Role::pluck('display_name','name')->all();        
        $genders = ['M' => 'Masculino', 'F' => 'Feminino'];
        $states = states();
        $banks = banks()->pluck('title', 'id');
        $accountTypes = ['Conta Corrente' => 'Conta Corrente', 'Conta Poupança' => 'Conta Poupança'];
        $products = Product::where('is_active', 1)->get();

        return view('affiliate.create',compact('user','roles','parentUsers', 'genders', 'states', 'banks', 'accountTypes', 'showRoles', 'products'));;
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {       
        return $this->networkRepository->store($request);
    }
    
}
