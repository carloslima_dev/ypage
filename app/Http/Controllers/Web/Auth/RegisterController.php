<?php

namespace App\Http\Controllers\Web\Auth;

use App\User;
use App\Models\UserAffiliate;
use App\Models\UserAddress;
use App\Models\UserBank;
use App\Models\UserPhone;

use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => 'required|same:confirm-password',
            'roles' => ['required', Rule::notIn('admin')],
            'userAffiliate.data_nascimento' => 'required',
            'userAffiliate.sexo' => 'required',
            'userAffiliate.rg' => ['required', Rule::unique('user_affiliates', 'rg')],
            'userAffiliate.orgao_emissor' => 'required',
            'userAffiliate.cpf' => ['required', Rule::unique('user_affiliates', 'cpf')],
            'userBank.bank_id' =>  'required',
            'userBank.agency' =>  'required',
            'userBank.account' =>  'required',
            'userBank.account_type' =>  'required',
            'userBank.holder_document' =>  'required',
            'userBank.holder_name' =>  'required',            
        ]);
    }
    
    public function showRegistrationForm()
    {
        $parentUsers = parents();
        $genders = ['M' => 'Masculino', 'F' => 'Feminino'];
        $states = states();
        $banks = banks()->pluck('title', 'id');
        $accountTypes = ['Conta Corrente' => 'Conta Corrente', 'Conta Poupança' => 'Conta Poupança'];
        return view('auth.register',compact('user','roles','parentUsers', 'genders', 'states', 'banks', 'accountTypes'));

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);       
        
        $input = $data;

        if ($user) {
                        
            $user->assignRole($data['roles']);
            
            //create or update userAffiliate
            $input['userAffiliate']['user_id'] = $user->id;
            $input['userAffiliate']['referrer_id'] = $input['userAffiliate']['parent_id'];
            
            UserAffiliate::create($input['userAffiliate']);
            
            //create or update userAddress
            if (!empty($input['userAddress'])) {
                foreach ($input['userAddress'] as $address) {
                    $address['user_id'] = $user->id;
                    UserAddress::create($address);
                }
            }

            //create or update userPhones
            if (!empty($input['userPhone'])) {
                foreach ($input['userPhone'] as $phone) {
                    $phone['user_id'] = $user->id;
                    UserPhone::create($phone);
                }
            }
                        
            $input['userBank']['user_id'] = $user->id;

            UserBank::create($input['userBank']);
        }
        return $user;
    }
}
