<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Models\UserAffiliate;
use App\Models\UserAddress;
use App\Models\UserBank;
use App\Models\UserPhone;
use Spatie\Permission\Models\Role;

use DB;
use Hash;
use Auth;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
         $this->middleware('permission:user-list');
         $this->middleware('permission:user-create', ['only' => ['create','store']]);
         $this->middleware('permission:user-edit', ['only' => ['edit','update']]); 
         $this->middleware('permission:user-delete', ['only' => ['destroy']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = User::select([
            'users.id',
            'users.name as name',
            'parent.name as parent',
            'users.email'
        ])
        ->leftjoin('user_affiliates', 'user_id', '=', 'users.id')
        ->leftjoin('users as parent', 'user_affiliates.parent_id', '=', 'parent.id')
        ->orderBy('users.name','ASC')->paginate(10);
        
        if (!empty($request->input('list'))) {
            return $data;
        }
        return view('admin.users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('display_name','name')->all();
        $showRoles = true;
        $parentUsers = parents();
        $genders = ['M' => 'Masculino', 'F' => 'Feminino'];
        $states = states();
        $banks = banks()->pluck('title', 'id');
        $accountTypes = ['Conta Corrente' => 'Conta Corrente', 'Conta Poupança' => 'Conta Poupança'];
        return view('admin.users.create',compact('user','roles','parentUsers', 'genders', 'states', 'banks', 'accountTypes', 'showRoles'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => 'required|same:confirm-password',
            'roles' => ['required', Rule::notIn('admin')],
            'userAffiliate.data_nascimento' => 'required',
            'userAffiliate.sexo' => 'required',
            'userAffiliate.rg' => ['required', Rule::unique('user_affiliates', 'rg')],
            'userAffiliate.orgao_emissor' => 'required',
            'userAffiliate.cpf' => ['required', Rule::unique('user_affiliates', 'cpf')],
            'userBank.bank_id' =>  'required',
            'userBank.agency' =>  'required',
            'userBank.account' =>  'required',
            'userBank.account_type' =>  'required',
            'userBank.holder_document' =>  'required',
            'userBank.holder_name' =>  'required',            
        ]);


        $input = $request->all();
        $input['password'] = Hash::make($input['password']);


        $user = User::create($input);
        $user->assignRole($request->input('roles'));


        if ($user) {
                                    
            //create or update userAffiliate
            $input['userAffiliate']['user_id'] = $user->id;
            $input['userAffiliate']['referrer_id'] = $input['userAffiliate']['parent_id'];
            UserAffiliate::create($input['userAffiliate']);
            
            //create or update userAddress
            if (!empty($input['userAddress'])) {
                foreach ($input['userAddress'] as $address) {
                    $address['user_id'] = $user->id;
                    UserAddress::create($address);
                }
            }

            //create or update userPhones
            if (!empty($input['userPhone'])) {
                foreach ($input['userPhone'] as $phone) {
                    $phone['user_id'] = $user->id;
                    UserPhone::create($phone);
                }
            }
                        
            $input['userBank']['user_id'] = $user->id;

            UserBank::create($input['userBank']);
        }
        
        return redirect()->route('users.index')->with('success', trans('general.user_created_successfully'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('admin.users.show',compact('user'));
    }

    /**
     * Display the profile user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        $user = User::find(Auth::user()->id);
        return view('admin.users.show',compact('user'));
    }    

    /**'roles' => 'required'
     * Show the form for editing the specified resource.
     *
     * @param  int  $id $('#user form').
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::with(['userAffiliate', 'userAddress', 'userPhone.type'])->where('id', $id)->first();
        $roles = Role::pluck('display_name','name')->all();
        $userRole = $user->roles->pluck('name','name')->all();
        $parentUsers = parents();
        $genders = ['M' => 'Masculino', 'F' => 'Feminino'];
        $states = states();
        $banks = banks()->pluck('title', 'id');
        $accountTypes = ['Conta Corrente' => 'Conta Corrente', 'Conta Poupança' => 'Conta Poupança'];
        return view('admin.users.edit',compact('user','roles','userRole','parentUsers', 'genders', 'states', 'banks', 'accountTypes'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
            'roles' => 'required',
            'userAffiliate.data_nascimento' => 'required',
            'userAffiliate.sexo' => 'required',
            'userAffiliate.rg' => ['required', Rule::unique('user_affiliates', 'rg')->ignore($id, 'user_id')],
            'userAffiliate.orgao_emissor' => 'required',
            'userAffiliate.cpf' => ['required', Rule::unique('user_affiliates', 'cpf')->ignore($id, 'user_id')],
            'userBank.bank_id' =>  'required',
            'userBank.agency' =>  'required',
            'userBank.account' =>  'required',
            'userBank.account_type' =>  'required',
            'userBank.holder_document' =>  'required',
            'userBank.holder_name' =>  'required',
        ]);


        $input = $request->all();

        if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = array_except($input,array('password'));    
        }

        $user = User::find($id);
        if ($user->update($input)) {
            
            DB::table('model_has_roles')->where('model_id',$id)->delete();
            $user->assignRole($request->input('roles'));
            
            //create or update userAffiliate
            $userAffiliate = UserAffiliate::where('user_id', $id)->first();            
            $input['userAffiliate']['user_id'] = $id;

            if (empty($userAffiliate)) {
                UserAffiliate::create($input['userAffiliate']);
            } else {
                $userAffiliate->update($input['userAffiliate']);
            }
            
            //create or update userAddress
            if (!empty($input['userAddress'])) {
                foreach ($input['userAddress'] as $address) {
                    $address['user_id'] = $id;
                    $userAddress = UserAddress::find($address['id']);
                    if (empty($userAddress)) {
                        UserAddress::create($address);
                    } else {
                        $userAddress->update($address);
                    }
                }
            }

            //create or update userPhones
            if (!empty($input['userPhone'])) {
                foreach ($input['userPhone'] as $phone) {
                    $phone['user_id'] = $id;
                    $userPhone = UserPhone::find($phone['id']);
                    if (empty($userPhone)) {
                        UserPhone::create($phone);
                    } else {
                        $userPhone->update($phone);
                    }
                }
            }
            
            $userBank = UserBank::where('user_id', $id)->first();
            
            $input['userBank']['user_id'] = $id;

            if (empty($userBank)) {
                UserBank::create($input['userBank']);
            } else {
                $userBank->update($input['userBank']);
            }            
            return redirect()->route('users.index')
                        ->with('success', trans('general.user_updated_successfully'));
            
        }       

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return ["success" => "User $id deleted successfully"];
    }
}
