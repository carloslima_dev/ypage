<?php
use App\User;
use App\Models\Bank;
use App\Models\Setting;

function states() {
    return [
        'AC' => 'Acre',
        'AL' => 'Alagoas',
        'AP' => 'Amapá',
        'AM' => 'Amazonas',
        'BA' => 'Bahia',
        'CE' => 'Ceará',
        'DF' => 'Distrito Federal',
        'ES' => 'Espírito Santo',
        'GO' => 'Goiás',
        'MA' => 'Maranhão',
        'MT' => 'Mato Grosso',
        'MS' => 'Mato Grosso do Sul',
        'MG' => 'Minas Gerais',
        'PA' => 'Pará',
        'PB' => 'Paraíba',
        'PR' => 'Paraná',
        'PE' => 'Pernambuco',
        'PI' => 'Piauí',
        'RJ' => 'Rio de Janeiro',
        'RN' => 'Rio Grande do Norte',
        'RS' => 'Rio Grande do Sul',
        'RO' => 'Rondônia',
        'RR' => 'Roraima',
        'SC' => 'Santa Catarina',
        'SP' => 'São Paulo',
        'SE' => 'Sergipe',
        'TO' => 'Tocantins'
    ];
}

function parents()
{
    return User::select(DB::raw("CONCAT('#', id,' - ',name) AS name"),'id')->pluck('name', 'id')->all();
}

function banks()
{
    return Bank::orderBy('use_frequency', 'desc')->orderBy('title', 'asc')->get();
}

function checkIfIsAdmin($userId = null)
 {
    $roles = \App\User::find(!empty($userId) ? $userId : Auth::user()->id)->getRoleNames();
    foreach ($roles as $role) {
        if ($role == 'admin') {
            return true;
        }
    }
    return false;
}

function getSetting($settingName = null)
{
    $settings = Setting::all();
    if (!empty($settingName)) {
        foreach ($settings as $setting) {
            if ($setting->setting_name == $settingName) {
                return $setting->setting_value;
            }
        }
    }
    return $settings;
}