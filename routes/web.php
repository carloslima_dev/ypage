<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();
Auth::routes(['verify' => true]);

Route::get('/', 'HomeController@index')->name('dashboard');

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth', 'verified']], function() {
    Route::resource('roles','RoleController');
    Route::resource('users','UserController');        
    Route::resource('products','ProductController');
    
    Route::post('settings/upload', 'SettingController@upload')->name('settings.upload');
    Route::resource('settings','SettingController');
    Route::post('products/upload', 'ProductController@upload')->name('products.upload');
    
    Route::get('perfil', 'UserController@profile')->name('profile');
    
    Route::group(['prefix' => 'network'], function() {
        Route::get('laterality', 'NetworkController@laterality')->name('network.laterality');        
        Route::get('create', 'NetworkController@create')->name('network.create');
        Route::post('store', 'NetworkController@store')->name('network.store');
        Route::get('distribution', 'NetworkController@distribution')->name('network.distribution');
        Route::get('sponsor', 'NetworkController@sponsor')->name('network.sponsor');
    });
});